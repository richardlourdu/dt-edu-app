var request = require('request');
var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {

    app.post('/student', (req, res) => {
        console.log(req.body);
        //custom_canvas_assignment_id
        const assignmentId = req.body.custom_canvas_assignment_id;
        const map = req.body;
        db.collection('student').insert(map, (err, result) => {
          //window.open("http://dt-edu-dev-janus.airislabs.com/survey-widget/dist/","_self");
          if (err) { 
            res.send({ 'error': 'An error has occurred' }); 
            console.log("Error!!!!");
          } else {
           request.get("http://dt-edu-dev-janus.airislabs.com:8080/map/"+assignmentId, function (error, response, body) {
            //request.get("http://localhost/map/"+assignmentId, function (error, response, body) {  
            /*console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); 
            */
            console.log('statusCode:', response && response.statusCode);
            
            if(response && response.statusCode==200 && body!={})
            {
              var mapObj = JSON.parse(body);
              var surveyId = mapObj["survey_id"]; 
              var redirectUrl = "http://dt-edu-dev-janus.airislabs.com/survey-widget/dist?surveyId="+surveyId;
              //console.log("redirect url ", redirectUrl);
              res.redirect(redirectUrl);
            }
            //if there is no valid surveyId, 
            else
            {
              var redirectUrl = "http://dt-edu-dev-janus.airislabs.com/survey-widget/dist?surveyId=404";
              console.log("redirect url ", redirectUrl);
              res.redirect(redirectUrl);
            }
            
          }); 
          }
        });
      });
    };