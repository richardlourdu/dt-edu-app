var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {

    app.post('/map', (req, res) => {
        //console.log(req.body);
        const map = req.body;
        db.collection('map').insert(map, (err, result) => {
          //window.open("http://dt-edu-dev-janus.airislabs.com/survey-widget/dist/","_self");
          if (err) { 
            res.send({ 'error': 'An error has occurred' }); 
            console.log("Error!!!!");
          } else {
            //res.redirect("http://dt-edu-dev-janus.airislabs.com/survey-widget/dist/");
            res.send(result.ops[0]);
          }
        });
      });

      app.get('/map', (req, res) => {
    
        var resultArray = []; 
        var map = db.collection('map').find();    
        map.forEach(function(doc, err){
        resultArray.push(doc); 
        }, function(){
          res.send(resultArray);
        });

      });

    app.get('/map/course/:courseId', (req, res) => {
    
          var resultArray = []; 
          const courseId = req.params.courseId.toString();  
          const details = {'course_id': courseId};
          
          var map = db.collection('map').find(details);    
          map.forEach(function(doc, err){
          resultArray.push(doc); 

          }, function(){
            res.send(resultArray);
          });

        });

    app.get('/map/:id', (req, res) => {

    const id = req.params.id.toString();    
    //const details = { '_id': new ObjectID(id) };
    const details = { '_id': id};
    
    db.collection('map').findOne(details, (err, item) => {
        if (err) {
          res.send({'error':'An error has occurred'});
        } else {
          
          if(item)
          res.send(item);
          else
          res.send({});
        } 
      });
      
    });


  app.put('/map/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    const map = req.body;
    db.collection('map').update(details, map, (err, result) => {
      if (err) {
          res.send({'error':'An error has occurred'});
      } else {
          res.send(map);
      } 
    });
  });


  app.delete('/map/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': id };
    db.collection('map').remove(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'});
      } else {
        res.send('map ' + id + ' deleted!');
      } 
    });
  });

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
};