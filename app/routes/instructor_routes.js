var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {

    app.post('/instructor', (req, res) => {
        console.log("instructor API");
        const courseId = req.body.custom_canvas_course_id;
        const map = req.body;
        db.collection('instructor').insert(map, (err, result) => {
          if (err) { 
            res.send({ 'error': 'An error has occurred' }); 
            console.log("Error!!!!");
          } else {
            //courseId = 1654557  
            var redirectUrl = "http://dt-edu-dev-janus.airislabs.com/instructor-app/dist?courseId="+courseId;
            console.log(redirectUrl);
            res.redirect(redirectUrl);
            //res.send(result.ops[0]);
          }
        });
      });
    };