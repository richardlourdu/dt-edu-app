// routes/index.js
const mapRoutes = require('./map_routes');
const studentRoutes = require('./student_routes');
const instructorRoutes = require('./instructor_routes');
module.exports = function(app, db) {
  mapRoutes(app, db);
  studentRoutes(app, db);
  instructorRoutes(app, db);
  // Other route groups could go here, in the future
};