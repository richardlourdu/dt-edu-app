const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const db             = require('./config/db');
const app            = express();
var cors = require('cors');

const port = 8080;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
MongoClient.connect(db.url, (err, database) => {
  if (err) return console.log(err)
  
  // Make sure you add the database name and not the collection name
  //const database = database.db("map-api")
  require('./app/routes')(app, database);

  app.listen(port, () => {
    console.log('We are live on ' + port);
  });      
  /*
  app.listen(process.env.PORT || port , () => {
       console.log('We are live on ' + process.env.PORT);
      });
  */    
  
})